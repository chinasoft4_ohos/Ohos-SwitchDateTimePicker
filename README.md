# Ohos-SwitchDateTimePicker

#### 项目介绍
- 项目名称：Ohos-SwitchDateTimePicker
- 所属系列：openharmony的第三方组件适配移植
- 功能：用于在同一 UI 中使用 DatePicker（日历）和 TimePicker（时钟）在对话框中选择日期对象
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6,DevEco Studio 2.2 Beta1
- 基线版本：Release 2.0

#### 效果演示
<img src="img/demo.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
      implementation('com.gitee.chinasoft_ohos:Ohos-SwitchDateTimePicker:1.0.1')
    ......  
 }
 ```

在sdk6,DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
1、初始化SwitchDateTimeDialogFragment
```java
SwitchDateTimeDialogFragment dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                getAbility(),
                getString(ResourceTable.String_label_datetime_dialog),
                "OK",
                "Cancel",
                getString(ResourceTable.String_clean) // Optional
        );
```
2、设置日期
```java
        // Optionally define a timezone
        dateTimeFragment.setTimeZone(TimeZone.getDefault());

        // Init format
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("d MMM yyyy HH:mm", java.util.Locale.getDefault());
        // Assign unmodifiable values
        dateTimeFragment.set24HoursMode(false);
        dateTimeFragment.setHighlightAMPMSelection(false);
        dateTimeFragment.setMinimumDateTime(new GregorianCalendar(2015, Calendar.JANUARY, 1).getTime());
        dateTimeFragment.setMaximumDateTime(new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime());

        // Define new day and month format
        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {

            e.fillInStackTrace();
        }
```
3、设置回调监听
```java
        // Set listener for date
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
               textView.setText(myDateFormat.format(date));
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
                // Optional if neutral button does'nt exists
                textView.setText("");
            }
        });
```
4、显示日历组件
```java
       dateTimeFragment.startAtCalendarView();
       dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, 
                        Calendar.MARCH, 4, 15, 20).getTime());
       dateTimeFragment.show();
```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.1

#### 版权和许可信息
```
Copyright (c) 2016 JAMET Jeremy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```