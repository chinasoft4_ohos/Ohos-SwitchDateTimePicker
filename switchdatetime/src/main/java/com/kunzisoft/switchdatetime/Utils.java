package com.kunzisoft.switchdatetime;

import ohos.accessibility.AccessibilitySystemAbilityClient;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class Utils {

    private static final int ANIMATION_DELAY = 0;
    private static final int PULSE_ANIMATOR_DURATION = 544;

    public static final String CH_MOUTH[] = {"一月", "二月", "三月", "四月",
            "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"};
    public static final String EN_MOUTH[] = {"January", "February", "March", "April",
            "May", "June", "July", "August", "September", "October", "November", "December"};

    /**
     * animLabelElement
     *
     * @param component component
     */
    public static void animLabelElement(Component component) {
        AnimatorProperty monthDayAnim = getPulseAnimator(component, 0.9F, 1.05F);
        monthDayAnim.setDelay(ANIMATION_DELAY);
        monthDayAnim.start();
    }

    /**
     * getPulseAnimator
     *
     * @param labelToAnimate labelToAnimate
     * @param decreaseRatio  decreaseRatio
     * @param increaseRatio  increaseRatio
     * @return pulseAnimator pulseAnimator
     */
    public static AnimatorProperty getPulseAnimator(Component labelToAnimate, float decreaseRatio, float increaseRatio) {
        AnimatorProperty pulseAnimator = new AnimatorProperty(labelToAnimate);
        pulseAnimator.scaleXFrom(decreaseRatio).scaleYFrom(decreaseRatio).scaleX(1).scaleY(1).setDuration(PULSE_ANIMATOR_DURATION);
        pulseAnimator.setCurveType(Animator.CurveType.ACCELERATE);

        return pulseAnimator;
    }

    /**
     * 是否触摸
     *
     * @param accessibilityManager accessibilityManager
     * @return accessibilityManager.isAccessibilityCaptionEnabled() accessibilityManager.isAccessibilityCaptionEnabled()
     */
    public static boolean isTouchExplorationEnabled(AccessibilitySystemAbilityClient accessibilityManager) {
        return accessibilityManager.isAccessibilityCaptionEnabled();
    }

    /**
     * Try to speak the specified text, for accessibility. Only available on JB or later.
     *
     * @param text Text to announce.
     * @param view view
     */
    public static void tryAccessibilityAnnounce(Component view, String text) {
        assert view != null;
        view.announceAccessibility(text);
    }

    /**
     * vp2px
     *
     * @param context context
     * @param vp      vp
     * @return vp2px vp
     */
    public static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (vp * attributes.densityPixels + 0.5f);
    }
}
