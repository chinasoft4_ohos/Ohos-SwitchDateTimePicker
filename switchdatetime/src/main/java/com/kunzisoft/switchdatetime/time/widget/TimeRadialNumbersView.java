package com.kunzisoft.switchdatetime.time.widget;
/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.kunzisoft.switchdatetime.ResourceTable;
import com.kunzisoft.switchdatetime.date.utils.AttrUtils;
import com.kunzisoft.switchdatetime.date.utils.LogUtil;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * A view to show a series of numbers in a circular pattern.
 */
public class TimeRadialNumbersView extends Component implements Component.DrawTask {
    private final static String TAG = "RadialTextsView";

    private final Paint mPaint = new Paint();

    private boolean mDrawValuesReady;
    private boolean mIsInitialized;

    private Font mTypefaceLight;
    private Font mTypefaceRegular;
    private String[] mTexts;
    private String[] mInnerTexts;
    private boolean mIs24HourMode;
    private boolean mHasInnerCircle;
    private float mCircleRadiusMultiplier;
    private float mAmPmCircleRadiusMultiplier;
    private float mNumbersRadiusMultiplier;
    private float mInnerNumbersRadiusMultiplier;
    private float mTextSizeMultiplier;
    private float mInnerTextSizeMultiplier;

    private int mXCenter;
    private int mYCenter;
    private float mCircleRadius;
    private boolean mTextGridValuesDirty;
    private float mTextSize;
    private float mInnerTextSize;
    private float[] mTextGridHeights;
    private float[] mTextGridWidths;
    private float[] mInnerTextGridHeights;
    private float[] mInnerTextGridWidths;

    private float mAnimationRadiusMultiplier;
    private float mTransitionMidRadiusMultiplier;
    private float mTransitionEndRadiusMultiplier;
    AnimatorGroup mDisappearAnimator;
    AnimatorGroup mReappearAnimator;
    private InvalidateUpdateListener mInvalidateUpdateListener;

    /**
     * 构造方法
     *
     * @param context context
     */
    public TimeRadialNumbersView(Context context) {
        this(context, null, 0);
    }

    /**
     * TimeRadialNumbersView
     *
     * @param context context
     * @param attrs   attrs
     */
    public TimeRadialNumbersView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * TimeRadialNumbersView
     *
     * @param context      context
     * @param attrs        attrs
     * @param defStyleAttr defStyleAttr
     */
    public TimeRadialNumbersView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addDrawTask(this);
        mIsInitialized = false;

        setNumbersColor(AttrUtils.getColorFromAttr(attrs, "timeCircularNumbersColor", Color.BLACK.getValue()));

        mPaint.setAntiAlias(true);
    }

    /**
     * initialize
     *
     * @param context       context
     * @param texts         texts
     * @param innerTexts    innerTexts
     * @param is24HourMode  is24HourMode
     * @param disappearsOut disappearsOut
     */
    public void initialize(Context context, String[] texts, String[] innerTexts,
                           boolean is24HourMode, boolean disappearsOut) {
        if (mIsInitialized) {
            LogUtil.e("This RadialTextsView may only be initialized once.");
            return;
        }

        // Set up the paint.
        String typefaceFamily = context.getString(ResourceTable.String_radial_numbers_typeface);
        Font.Builder builder = new Font.Builder(typefaceFamily);
        mTypefaceLight = builder.build();
        String typefaceFamilyRegular = context.getString(ResourceTable.String_sans_serif);
        Font.Builder builderRegular = new Font.Builder(typefaceFamilyRegular);
        mTypefaceRegular = builderRegular.build();
        mPaint.setAntiAlias(true);
        mPaint.setTextAlign(TextAlignment.CENTER);

        mTexts = texts;
        mInnerTexts = innerTexts;
        mIs24HourMode = is24HourMode;
        mHasInnerCircle = (innerTexts != null);

        // Calculate the radius for the main circle.
        if (is24HourMode) {
            mCircleRadiusMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_circle_radius_multiplier_24HourMode));
        } else {
            mCircleRadiusMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_circle_radius_multiplier));
            mAmPmCircleRadiusMultiplier =
                    Float.parseFloat(context.getString(ResourceTable.Float_ampm_circle_radius_multiplier));
        }

        // Initialize the widths and heights of the grid, and calculate the values for the numbers.
        mTextGridHeights = new float[7];
        mTextGridWidths = new float[7];
        if (mHasInnerCircle) {
            mNumbersRadiusMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_numbers_radius_multiplier_outer));
            mTextSizeMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_text_size_multiplier_outer));
            mInnerNumbersRadiusMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_numbers_radius_multiplier_inner));
            mInnerTextSizeMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_text_size_multiplier_inner));

            mInnerTextGridHeights = new float[7];
            mInnerTextGridWidths = new float[7];
        } else {
            mNumbersRadiusMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_numbers_radius_multiplier_normal));
            mTextSizeMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_text_size_multiplier_normal));
        }

        mAnimationRadiusMultiplier = 1;
        mTransitionMidRadiusMultiplier = 1f + (0.05f * (disappearsOut ? -1 : 1));
        mTransitionEndRadiusMultiplier = 1f + (0.2f * (disappearsOut ? 1 : -1));
        mInvalidateUpdateListener = new InvalidateUpdateListener();

        mTextGridValuesDirty = true;
        mIsInitialized = true;
    }

    /**
     * Used by the animation to move the numbers in and out.
     *
     * @param animationRadiusMultiplier animationRadiusMultiplier
     */
    public void setAnimationRadiusMultiplier(float animationRadiusMultiplier) {
        mAnimationRadiusMultiplier = animationRadiusMultiplier;
        mTextGridValuesDirty = true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int viewWidth = getWidth();
        if (viewWidth == 0 || !mIsInitialized) {
            return;
        }

        if (!mDrawValuesReady) {
            mXCenter = getWidth() / 2;
            mYCenter = getHeight() / 2;
            mCircleRadius = Math.min(mXCenter, mYCenter) * mCircleRadiusMultiplier;
            if (!mIs24HourMode) {
                // We'll need to draw the AM/PM circles, so the main circle will need to have
                // a slightly higher center. To keep the entire view centered vertically, we'll
                // have to push it up by half the radius of the AM/PM circles.
                float amPmCircleRadius = mCircleRadius * mAmPmCircleRadiusMultiplier;
                mYCenter -= amPmCircleRadius / 2;
            }

            mTextSize = mCircleRadius * mTextSizeMultiplier;
            if (mHasInnerCircle) {
                mInnerTextSize = mCircleRadius * mInnerTextSizeMultiplier;
            }

            // Because the text positions will be static, pre-render the animations.
            renderAnimations();

            mTextGridValuesDirty = true;
            mDrawValuesReady = true;
        }

        // Calculate the text positions, but only if they've changed since the last onDraw.
        if (mTextGridValuesDirty) {
            float numbersRadius =
                    mCircleRadius * mNumbersRadiusMultiplier * mAnimationRadiusMultiplier;

            // Calculate the positions for the 12 numbers in the main circle.
            calculateGridSizes(numbersRadius, mXCenter, mYCenter,
                    mTextSize, mTextGridHeights, mTextGridWidths);
            if (mHasInnerCircle) {
                // If we have an inner circle, calculate those positions too.
                float innerNumbersRadius =
                        mCircleRadius * mInnerNumbersRadiusMultiplier * mAnimationRadiusMultiplier;
                calculateGridSizes(innerNumbersRadius, mXCenter, mYCenter,
                        mInnerTextSize, mInnerTextGridHeights, mInnerTextGridWidths);
            }
            mTextGridValuesDirty = false;
        }

        // Draw the texts in the pre-calculated positions.
        drawTexts(canvas, mTextSize, mTypefaceLight, mTexts, mTextGridWidths, mTextGridHeights);
        if (mHasInnerCircle) {
            drawTexts(canvas, mInnerTextSize, mTypefaceRegular, mInnerTexts,
                    mInnerTextGridWidths, mInnerTextGridHeights);
        }
    }

    /**
     * calculateGridSizes
     *
     * @param numbersRadius   numbersRadius
     * @param xCenter         xCenter
     * @param yCenter         yCenter
     * @param textSize        textSize
     * @param textGridHeights textGridHeights
     * @param textGridWidths  textGridWidths
     */
    private void calculateGridSizes(float numbersRadius, float xCenter, float yCenter,
                                    float textSize, float[] textGridHeights, float[] textGridWidths) {
        /*
         * The numbers need to be drawn in a 7x7 grid, representing the points on the Unit Circle.
         */
        float offset1 = numbersRadius;
        // cos(30) = a / r => r * cos(30) = a => r * √3/2 = a
        float offset2 = numbersRadius * ((float) Math.sqrt(3)) / 2f;
        // sin(30) = o / r => r * sin(30) = o => r / 2 = a
        float offset3 = numbersRadius / 2f;
        mPaint.setTextSize((int) textSize);
        // We'll need yTextBase to be slightly lower to account for the text's baseline.
        yCenter -= (mPaint.descent() + mPaint.ascent()) / 2;

        textGridHeights[0] = yCenter - offset1;
        textGridWidths[0] = xCenter - offset1;
        textGridHeights[1] = yCenter - offset2;
        textGridWidths[1] = xCenter - offset2;
        textGridHeights[2] = yCenter - offset3;
        textGridWidths[2] = xCenter - offset3;
        textGridHeights[3] = yCenter;
        textGridWidths[3] = xCenter;
        textGridHeights[4] = yCenter + offset3;
        textGridWidths[4] = xCenter + offset3;
        textGridHeights[5] = yCenter + offset2;
        textGridWidths[5] = xCenter + offset2;
        textGridHeights[6] = yCenter + offset1;
        textGridWidths[6] = xCenter + offset1;
    }

    /**
     * Draw the 12 text values at the positions specified by the textGrid parameters.
     *
     * @param canvas          canvas
     * @param textSize        textSize
     * @param typeface        typeface
     * @param texts           texts
     * @param textGridWidths  textGridWidths
     * @param textGridHeights textGridHeights
     */
    private void drawTexts(Canvas canvas, float textSize, Font typeface, String[] texts,
                           float[] textGridWidths, float[] textGridHeights) {
        mPaint.setTextSize((int) textSize);
        mPaint.setFont(typeface);
        canvas.drawText(mPaint, texts[0], textGridWidths[3], textGridHeights[0]);
        canvas.drawText(mPaint, texts[1], textGridWidths[4], textGridHeights[1]);
        canvas.drawText(mPaint, texts[2], textGridWidths[5], textGridHeights[2]);
        canvas.drawText(mPaint, texts[3], textGridWidths[6], textGridHeights[3]);
        canvas.drawText(mPaint, texts[4], textGridWidths[5], textGridHeights[4]);
        canvas.drawText(mPaint, texts[5], textGridWidths[4], textGridHeights[5]);
        canvas.drawText(mPaint, texts[6], textGridWidths[3], textGridHeights[6]);
        canvas.drawText(mPaint, texts[7], textGridWidths[2], textGridHeights[5]);
        canvas.drawText(mPaint, texts[8], textGridWidths[1], textGridHeights[4]);
        canvas.drawText(mPaint, texts[9], textGridWidths[0], textGridHeights[3]);
        canvas.drawText(mPaint, texts[10], textGridWidths[1], textGridHeights[2]);
        canvas.drawText(mPaint, texts[11], textGridWidths[2], textGridHeights[1]);
    }

    /**
     * Render the animations for appearing and disappearing.
     */
    private void renderAnimations() {
        // Set up animator for disappearing.
        AnimatorProperty outScale = new AnimatorProperty(this);
        outScale.scaleXFrom(1).scaleYFrom(1)
                .scaleX(mTransitionEndRadiusMultiplier).scaleY(mTransitionEndRadiusMultiplier);

        AnimatorProperty outAlpha = new AnimatorProperty(this);
        outAlpha.alphaFrom(1).alpha(0);

        mDisappearAnimator = new AnimatorGroup();
        mDisappearAnimator.runParallel(outScale, outAlpha);
        mDisappearAnimator.setDuration(500);
        mDisappearAnimator.setCurveType(Animator.CurveType.DECELERATE);
        mDisappearAnimator.setLoopedListener(mInvalidateUpdateListener);

        // Set up animator for reappearing.
        AnimatorProperty inScale = new AnimatorProperty(this);
        inScale.scaleXFrom(mTransitionEndRadiusMultiplier).scaleYFrom(mTransitionEndRadiusMultiplier)
                .scaleX(1).scaleY(1);

        AnimatorProperty inAlpha = new AnimatorProperty(this);
        inAlpha.alphaFrom(0).alpha(1);

        mReappearAnimator = new AnimatorGroup();
        mReappearAnimator.runParallel(inScale, inAlpha);
        mReappearAnimator.setDuration(500);
        mReappearAnimator.setCurveType(Animator.CurveType.ACCELERATE);
        mReappearAnimator.setLoopedListener(mInvalidateUpdateListener);
    }

    /**
     * getDisappearAnimator
     *
     * @return getDisappearAnimator getDisappearAnimator
     */
    public Animator getDisappearAnimator() {
        if (!mIsInitialized || !mDrawValuesReady || mDisappearAnimator == null) {
            LogUtil.e("RadialTextView was not ready for animation.");
            return null;
        }

        return mDisappearAnimator;
    }

    /**
     * getReappearAnimator
     *
     * @return getReappearAnimator getReappearAnimator
     */
    public Animator getReappearAnimator() {
        if (!mIsInitialized || !mDrawValuesReady || mReappearAnimator == null) {
            LogUtil.e("RadialTextView was not ready for animation.");
            return null;
        }

        return mReappearAnimator;
    }

    private class InvalidateUpdateListener implements Animator.LoopedListener {
        @Override
        public void onRepeat(Animator animator) {
            TimeRadialNumbersView.this.invalidate();
        }
    }

    public int getNumbersColor() {
        return mPaint.getColor().getValue();
    }

    /**
     * color
     *
     * @param color color
     */
    public void setNumbersColor(int color) {
        mPaint.setColor(new Color(color));
    }
}