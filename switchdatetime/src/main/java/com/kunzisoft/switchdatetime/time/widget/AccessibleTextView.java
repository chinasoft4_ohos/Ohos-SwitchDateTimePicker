package com.kunzisoft.switchdatetime.time.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * Fake Button class, used so TextViews can announce themselves as Buttons, for accessibility.
 */
public class AccessibleTextView extends Text {
    /**
     * AccessibleTextView
     *
     * @param context context
     * @param attrs   attrs
     */
    public AccessibleTextView(Context context, AttrSet attrs) {
        super(context, attrs);
    }
}