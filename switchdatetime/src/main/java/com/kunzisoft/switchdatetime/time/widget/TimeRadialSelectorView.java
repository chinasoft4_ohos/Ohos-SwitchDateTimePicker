package com.kunzisoft.switchdatetime.time.widget;
/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.kunzisoft.switchdatetime.ResourceTable;
import com.kunzisoft.switchdatetime.date.utils.AttrUtils;
import com.kunzisoft.switchdatetime.date.utils.LogUtil;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * View to show what number is selected. This will draw a blue circle over the number, with a blue
 * line coming from the center of the main circle to the edge of the blue selection.
 */
public class TimeRadialSelectorView extends Component implements Component.DrawTask {
    private static final String TAG = "RadialSelectorView";

    private final Paint mPaint = new Paint();

    private boolean mIsInitialized;
    private boolean mDrawValuesReady;

    private float mCircleRadiusMultiplier;
    private float mAmPmCircleRadiusMultiplier;
    private float mInnerNumbersRadiusMultiplier;
    private float mOuterNumbersRadiusMultiplier;
    private float mNumbersRadiusMultiplier;
    private float mSelectionRadiusMultiplier;
    private float mAnimationRadiusMultiplier;
    private boolean mIs24HourMode;
    private boolean mHasInnerCircle;

    private int mXCenter;
    private int mYCenter;
    private int mCircleRadius;
    private float mTransitionMidRadiusMultiplier;
    private float mTransitionEndRadiusMultiplier;
    private int mLineLength;
    private int mSelectionRadius;
    private InvalidateUpdateListener mInvalidateUpdateListener;

    private int mSelectionDegrees;
    private double mSelectionRadians;
    private boolean mForceDrawDot;

    /**
     * TimeRadialSelectorView
     *
     * @param context context
     */
    public TimeRadialSelectorView(Context context) {
        this(context, null, 0);
    }

    /**
     * TimeRadialSelectorView
     *
     * @param context context
     * @param attrs   attrs
     */
    public TimeRadialSelectorView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * TimeRadialSelectorView
     *
     * @param context      context
     * @param attrs        attrs
     * @param defStyleAttr defStyleAttr
     */
    public TimeRadialSelectorView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addDrawTask(this);
        mIsInitialized = false;

        setSelectorColor(AttrUtils.getColorFromAttr(attrs, "timeSelectorColor", Color.BLUE.getValue()));

        mPaint.setAntiAlias(true);
    }

    /**
     * Initialize this selector with the state of the picker.
     *
     * @param context Current context.
     * @param is24HourMode Whether the selector is in 24-hour mode, which will tell us
     * whether the circle's center is moved up slightly to make room for the AM/PM circles.
     * @param hasInnerCircle   Whether we have both an inner and an outer circle of numbers
     * that may be selected. Should be true for 24-hour mode in the hours circle.
     * @param disappearsOut Whether the numbers' animation will have them disappearing out or disappearing in.
     * @param selectionDegrees The initial degrees to be selected.
     * @param isInnerCircle Whether the initial selection is in the inner or outer circle.
     * Will be ignored when hasInnerCircle is false.
     */
    public void initialize(Context context, boolean is24HourMode, boolean hasInnerCircle,
                           boolean disappearsOut, int selectionDegrees, boolean isInnerCircle) {
        if (mIsInitialized) {
            LogUtil.e("This RadialSelectorView may only be initialized once.");
            return;
        }

        // Calculate values for the circle radius size.
        mIs24HourMode = is24HourMode;
        if (is24HourMode) {
            mCircleRadiusMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_circle_radius_multiplier_24HourMode));
        } else {
            mCircleRadiusMultiplier = Float.parseFloat(
                    context.getString(ResourceTable.Float_circle_radius_multiplier));
            mAmPmCircleRadiusMultiplier =
                    Float.parseFloat(context.getString(ResourceTable.Float_ampm_circle_radius_multiplier));
        }

        // Calculate values for the radius size(s) of the numbers circle(s).
        mHasInnerCircle = hasInnerCircle;
        if (hasInnerCircle) {
            mInnerNumbersRadiusMultiplier =
                    Float.parseFloat(context.getString(ResourceTable.Float_numbers_radius_multiplier_inner));
            mOuterNumbersRadiusMultiplier =
                    Float.parseFloat(context.getString(ResourceTable.Float_numbers_radius_multiplier_outer));
        } else {
            mNumbersRadiusMultiplier =
                    Float.parseFloat(context.getString(ResourceTable.Float_numbers_radius_multiplier_normal));
        }
        mSelectionRadiusMultiplier =
                Float.parseFloat(context.getString(ResourceTable.Float_selection_radius_multiplier));

        // Calculate values for the transition mid-way states.
        mAnimationRadiusMultiplier = 1;
        mTransitionMidRadiusMultiplier = 1f + (0.05f * (disappearsOut ? -1 : 1));
        mTransitionEndRadiusMultiplier = 1f + (0.2f * (disappearsOut ? 1 : -1));
        mInvalidateUpdateListener = new InvalidateUpdateListener();

        setSelection(selectionDegrees, isInnerCircle, false);
        mIsInitialized = true;
    }

    /**
     * Set the selection.
     *
     * @param selectionDegrees 要选择的度数。
     * @param isInnerCircle    选择应该在内圈还是外圈。如果 hasInnerCircle 初始化为 false，则将被忽略。
     * @param forceDrawDot     是否强制绘制选择圆中心的点。如果为 false，则仅当度数不是 30 的倍数时才会绘制点，即选择不在可见数字上。
     */
    public void setSelection(int selectionDegrees, boolean isInnerCircle, boolean forceDrawDot) {
        mSelectionDegrees = selectionDegrees;
        mSelectionRadians = selectionDegrees * Math.PI / 180;
        mForceDrawDot = forceDrawDot;

        if (mHasInnerCircle) {
            if (isInnerCircle) {
                mNumbersRadiusMultiplier = mInnerNumbersRadiusMultiplier;
            } else {
                mNumbersRadiusMultiplier = mOuterNumbersRadiusMultiplier;
            }
        }
    }

    /**
     * Set the multiplier for the radius. Will be used during animations to move in/out.
     *
     * @param animationRadiusMultiplier animationRadiusMultiplier
     */
    public void setAnimationRadiusMultiplier(float animationRadiusMultiplier) {
        mAnimationRadiusMultiplier = animationRadiusMultiplier;
    }

    /**
     * getDegreesFromCoords
     *
     * @param pointX        pointX
     * @param pointY        pointY
     * @param forceLegal    forceLegal
     * @param isInnerCircle isInnerCircle
     * @return degrees degrees
     */
    public int getDegreesFromCoords(float pointX, float pointY, boolean forceLegal, final Boolean[] isInnerCircle) {
        if (!mDrawValuesReady) {
            return -1;
        }

        double hypotenuse = Math.sqrt(
                (pointY - mYCenter) * (pointY - mYCenter) + (pointX - mXCenter) * (pointX - mXCenter));
        // Check if we're outside the range
        if (mHasInnerCircle) {
            if (forceLegal) {
                // If we're told to force the coordinates to be legal, we'll set the isInnerCircle
                // boolean based based off whichever number the coordinates are closer to.
                int innerNumberRadius = (int) (mCircleRadius * mInnerNumbersRadiusMultiplier);
                int distanceToInnerNumber = (int) Math.abs(hypotenuse - innerNumberRadius);
                int outerNumberRadius = (int) (mCircleRadius * mOuterNumbersRadiusMultiplier);
                int distanceToOuterNumber = (int) Math.abs(hypotenuse - outerNumberRadius);

                isInnerCircle[0] = (distanceToInnerNumber <= distanceToOuterNumber);
            } else {
                // Otherwise, if we're close enough to either number (with the space between the
                // two allotted equally), set the isInnerCircle boolean as the closer one.
                // appropriately, but otherwise return -1.
                int minAllowedHypotenuseForInnerNumber =
                        (int) (mCircleRadius * mInnerNumbersRadiusMultiplier) - mSelectionRadius;
                int maxAllowedHypotenuseForOuterNumber =
                        (int) (mCircleRadius * mOuterNumbersRadiusMultiplier) + mSelectionRadius;
                int halfwayHypotenusePoint = (int) (mCircleRadius *
                        ((mOuterNumbersRadiusMultiplier + mInnerNumbersRadiusMultiplier) / 2));

                if (hypotenuse >= minAllowedHypotenuseForInnerNumber && hypotenuse <= halfwayHypotenusePoint) {
                    isInnerCircle[0] = true;
                } else if (hypotenuse <= maxAllowedHypotenuseForOuterNumber && hypotenuse >= halfwayHypotenusePoint) {
                    isInnerCircle[0] = false;
                } else {
                    return -1;
                }
            }
        } else {
            // If there's just one circle, we'll need to return -1 if:
            // we're not told to force the coordinates to be legal, and
            // the coordinates' distance to the number is within the allowed distance.
            if (!forceLegal) {
                int distanceToNumber = (int) Math.abs(hypotenuse - mLineLength);
                // The max allowed distance will be defined as the distance from the center of the
                // number to the edge of the circle.
                int maxAllowedDistance = (int) (mCircleRadius * (1 - mNumbersRadiusMultiplier));
                if (distanceToNumber > maxAllowedDistance) {
                    return -1;
                }
            }
        }
        float opposite = Math.abs(pointY - mYCenter);
        double radians = Math.asin(opposite / hypotenuse);
        int degrees = (int) (radians * 180 / Math.PI);
        // Now we have to translate to the correct quadrant.
        boolean rightSide = (pointX > mXCenter);
        boolean topSide = (pointY < mYCenter);
        if (rightSide && topSide) {
            degrees = 90 - degrees;
        } else if (rightSide) {
            degrees = 90 + degrees;
        } else if (!topSide) {
            degrees = 270 - degrees;
        } else {
            degrees = 270 + degrees;
        }
        return degrees;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int viewWidth = getWidth();
        if (viewWidth == 0 || !mIsInitialized) {
            return;
        }

        if (!mDrawValuesReady) {
            mXCenter = getWidth() / 2;
            mYCenter = getHeight() / 2;
            mCircleRadius = (int) (Math.min(mXCenter, mYCenter) * mCircleRadiusMultiplier);

            if (!mIs24HourMode) {
                // We'll need to draw the AM/PM circles, so the main circle will need to have
                // a slightly higher center. To keep the entire view centered vertically, we'll
                // have to push it up by half the radius of the AM/PM circles.
                int amPmCircleRadius = (int) (mCircleRadius * mAmPmCircleRadiusMultiplier);
                mYCenter -= amPmCircleRadius / 2;
            }

            mSelectionRadius = (int) (mCircleRadius * mSelectionRadiusMultiplier);

            mDrawValuesReady = true;
        }

        // Calculate the current radius at which to place the selection circle.
        mLineLength = (int) (mCircleRadius * mNumbersRadiusMultiplier * mAnimationRadiusMultiplier);
        int pointX = mXCenter + (int) (mLineLength * Math.sin(mSelectionRadians));
        int pointY = mYCenter - (int) (mLineLength * Math.cos(mSelectionRadians));

        // Draw the selection circle.
        mPaint.setAlpha(0.195f);
        canvas.drawCircle(pointX, pointY, mSelectionRadius, mPaint);

        if (mForceDrawDot | mSelectionDegrees % 30 != 0) {
            // We're not on a direct tick (or we've been told to draw the dot anyway).
            mPaint.setAlpha(1);
            canvas.drawCircle(pointX, pointY, (mSelectionRadius * 2 / 7), mPaint);
        } else {
            // We're not drawing the dot, so shorten the line to only go as far as the edge of the
            // selection circle.
            int lineLength = mLineLength;
            lineLength -= mSelectionRadius;
            pointX = mXCenter + (int) (lineLength * Math.sin(mSelectionRadians));
            pointY = mYCenter - (int) (lineLength * Math.cos(mSelectionRadians));
        }

        // Draw the line from the center of the circle.
        mPaint.setAlpha(1);
        mPaint.setStrokeWidth(1);
        canvas.drawLine(mXCenter, mYCenter, pointX, pointY, mPaint);
    }

    /**
     * getDisappearAnimator
     *
     * @return getDisappearAnimator getDisappearAnimator
     */
    public Animator getDisappearAnimator() {
        if (!mIsInitialized || !mDrawValuesReady) {
            LogUtil.e("RadialSelectorView was not ready for animation.");
            return null;
        }

        AnimatorProperty outScale = new AnimatorProperty(this);
        outScale.scaleXFrom(1).scaleYFrom(1)
                .scaleX(mTransitionEndRadiusMultiplier).scaleY(mTransitionEndRadiusMultiplier);

        AnimatorProperty outAlpha = new AnimatorProperty(this);
        outAlpha.alphaFrom(1).alpha(0);

        AnimatorGroup disappearAnimator = new AnimatorGroup();
        disappearAnimator.runParallel(outScale, outAlpha);
        disappearAnimator.setDuration(500);
        disappearAnimator.setCurveType(Animator.CurveType.DECELERATE);
        disappearAnimator.setLoopedListener(mInvalidateUpdateListener);
        return disappearAnimator;
    }

    /**
     * getReappearAnimator
     *
     * @return null null
     */
    public Animator getReappearAnimator() {
        if (!mIsInitialized || !mDrawValuesReady) {
            LogUtil.e("RadialSelectorView was not ready for animation.");
            return null;
        }

        AnimatorProperty inScale = new AnimatorProperty(this);
        inScale.scaleXFrom(mTransitionEndRadiusMultiplier).scaleYFrom(mTransitionEndRadiusMultiplier)
                .scaleX(1).scaleY(1);

        AnimatorProperty inAlpha = new AnimatorProperty(this);
        inAlpha.alphaFrom(0).alpha(1);

        AnimatorGroup reappearAnimator = new AnimatorGroup();
        reappearAnimator.runParallel(inScale, inAlpha);
        reappearAnimator.setDuration(500);
        reappearAnimator.setCurveType(Animator.CurveType.ACCELERATE);
        reappearAnimator.setLoopedListener(mInvalidateUpdateListener);
        return reappearAnimator;
    }

    /**
     * We'll need to invalidate during the animation.
     */
    private class InvalidateUpdateListener implements Animator.LoopedListener {
        @Override
        public void onRepeat(Animator animator) {
            TimeRadialSelectorView.this.invalidate();
        }
    }

    public int getSelectorColor() {
        return mPaint.getColor().getValue();
    }

    /**
     * setSelectorColor
     *
     * @param color color
     */
    public void setSelectorColor(int color) {
        mPaint.setColor(new Color(color));
    }
}