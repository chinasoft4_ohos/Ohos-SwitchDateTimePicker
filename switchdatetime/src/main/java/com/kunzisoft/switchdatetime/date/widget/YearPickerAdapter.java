package com.kunzisoft.switchdatetime.date.widget;

import com.kunzisoft.switchdatetime.ResourceTable;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Adapter for manage elements of ListPickerYearView
 *
 * @author JJamet
 */
class YearPickerAdapter extends BaseItemProvider {

    private static final int LIST_ITEM_TYPE_STANDARD = 0;
    private static final int LIST_ITEM_TYPE_INDICATOR = 1;
    public static final int UNDEFINED = -1;
    private SimpleDateFormat yearFormat;
    private Calendar calendar;

    private List<Integer> listYears;
    private Integer selectedYear;
    private int positionSelectedYear;
    private int oldSelectedPosition = -1;

    private OnClickYearListener onClickYearListener;
    private Context mContext;

    /**
     * Initialize adapter with list of years and element selected
     *
     * @param context 上下文
     */
    YearPickerAdapter(Context context) {
        this.listYears = new ArrayList<>();
        this.selectedYear = UNDEFINED;
        this.yearFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
        this.calendar = Calendar.getInstance();
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return listYears.size();
    }

    @Override
    public Object getItem(int i) {
        return listYears.get(i);
    }

    @Override
    public long getItemId(int i) {
        return listYears.get(i);
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Integer currentYear = listYears.get(i);
        TextIndicatorViewHolder holder;

        if (component == null || oldSelectedPosition == i || positionSelectedYear == i) {
            component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_year_text, null, false);
            holder = new TextIndicatorViewHolder(component);
            component.setTag(holder);
        } else {
            holder = (TextIndicatorViewHolder) component.getTag();
        }

        calendar.set(Calendar.YEAR, currentYear);
        holder.textView.setText(yearFormat.format(calendar.getTime()));

        int viewType = getItemComponentType(i);
        switch (viewType) {
            case LIST_ITEM_TYPE_INDICATOR:
                holder.textView.setIndicator();
                holder.textView.setTextColor(Color.WHITE);
                break;
            default:
                holder.textView.setTextColor(new Color(mContext.getColor(ResourceTable.Color_textColorDefault)));
                break;
        }

        if (onClickYearListener != null) {
            holder.container.setClickedListener(new BufferYearClickListener(currentYear, i));
        }
        return component;
    }

    @Override
    public int getItemComponentType(int position) {
        if (listYears.get(position).equals(selectedYear)) {
            return LIST_ITEM_TYPE_INDICATOR;
        } else {
            return LIST_ITEM_TYPE_STANDARD;
        }
    }

    /**
     * Get the list of years
     *
     * @return years
     */
    public List<Integer> getListYears() {
        return listYears;
    }

    /**
     * Assign the list of years, replace current list
     *
     * @param listYears list of years
     */
    public void setListYears(List<Integer> listYears) {
        this.listYears = listYears;
    }
    /**
     * Get the current selected year or value of UNDEFINED if undefined
     *
     * @return selectedYear selectedYear
     */
    public int getSelectedYear() {
        return selectedYear;
    }

    /**
     * Assign the current selected year
     *
     * @param selectedYear year selected
     * @throws SelectYearException SelectYearException
     */

    public void setSelectedYear(int selectedYear) throws SelectYearException {
        if (!listYears.contains(selectedYear))
            throw new SelectYearException(selectedYear, listYears);
        this.selectedYear = selectedYear;
        oldSelectedPosition = positionSelectedYear;
        this.positionSelectedYear = listYears.indexOf(selectedYear);

    }
    /**
     * Get position of selected year or value of UNDEFINED if undefined
     *
     * @return positionSelectedYear positionSelectedYear
     */
    public int getPositionSelectedYear() {
        return positionSelectedYear;
    }

    /**
     * Get the listener called when the year is clicked
     *
     * @return onClickYearListener onClickYearListener
     */
    public OnClickYearListener getOnClickYearListener() {
        return onClickYearListener;
    }

    /**
     * Set the listener called when the year is clicked
     *
     * @param onClickYearListener year listener
     */
    public void setOnClickYearListener(OnClickYearListener onClickYearListener) {
        this.onClickYearListener = onClickYearListener;
    }

    /**
     * Listener when a click on Year item is performed
     */
    public interface OnClickYearListener {
        /**
         * Called on click
         *
         * @param component Current view clicked
         * @param year      Year of current item clicked
         * @param position  Position of item clicked
         */
        public void onItemYearClick(Component component, Integer year, int position);
    }

    private class BufferYearClickListener implements Component.ClickedListener {
        private Integer year;
        private int position;

        BufferYearClickListener(Integer yearClicked, int position) {
            this.year = yearClicked;
            this.position = position;
        }

        @Override
        public void onClick(Component component) {
            onClickYearListener.onItemYearClick(component, year, position);
        }
    }

    /**
     * Holder for TextIndicatorView
     */
    class TextIndicatorViewHolder {
        /**
         * of year
         */

        private ComponentContainer container;
        private TextCircularIndicatorView textView;

        TextIndicatorViewHolder(Component itemView) {
            container = (ComponentContainer) itemView.findComponentById(ResourceTable.Id_year_element_container);
            textView = (TextCircularIndicatorView) itemView.findComponentById(ResourceTable.Id_year_textView);
        }
    }

    class SelectYearException extends Exception {
        SelectYearException(Integer yearSelected, List<Integer> listYears) {
            super("Year selected " + yearSelected + " must be in list of years : " + listYears);
        }
    }
}
