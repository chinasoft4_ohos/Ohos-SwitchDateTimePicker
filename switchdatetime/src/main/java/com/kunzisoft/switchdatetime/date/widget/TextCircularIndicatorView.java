
package com.kunzisoft.switchdatetime.date.widget;

import com.kunzisoft.switchdatetime.date.utils.AttrUtils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * TextView with circular colored background
 *
 * @author JJamet
 */
public class TextCircularIndicatorView extends Text implements Component.DrawTask {

    private int mCircleColor = Color.BLUE.getValue();
    private Paint mCirclePaint = new Paint();

    /**
     * 构造方法
     *
     * @param context context
     */
    public TextCircularIndicatorView(Context context) {
        this(context, null, "0");
    }

    /**
     * 构造方法
     *
     * @param context context
     * @param attrs attrs
     */
    public TextCircularIndicatorView(Context context, AttrSet attrs) {
        this(context, attrs, "0");

    }

    /**
     * 构造方法
     *
     * @param context      context
     * @param attrs        attrs
     * @param defStyleAttr defStyleAttr
     */
    public TextCircularIndicatorView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }
    /**
     * Initialize constructor
     *
     * @param attrs attrs
     */
    private void init(AttrSet attrs) {
        if (attrs != null) {
            setCircleColor(AttrUtils.getColorFromAttr(attrs, "colorCircleIndicator", mCircleColor));
        }

        mCirclePaint.setFakeBoldText(true);
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setColor(new Color(mCircleColor));
        mCirclePaint.setTextAlign(TextAlignment.CENTER);
        mCirclePaint.setStyle(Paint.Style.FILL_STYLE);
    }

    @Override
    public CharSequence getComponentDescription() {
        return getText();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        int radius = Math.min(width, height) / 2;
        canvas.drawCircle(width / 2, height / 2, radius, mCirclePaint);
    }

    /**
     * Get color of background circle
     *
     * @return mCircleColor mCircleColor
     */
    public int getCircleColor() {
        return mCircleColor;
    }

    /**
     * Set color of background circle
     *
     * @param color color
     */
    public void setCircleColor(int color) {
        this.mCircleColor = color;
    }

    /**
     * setIndicator setIndicator
     */
    public void setIndicator() {
        addDrawTask(this, 1);
    }
}
