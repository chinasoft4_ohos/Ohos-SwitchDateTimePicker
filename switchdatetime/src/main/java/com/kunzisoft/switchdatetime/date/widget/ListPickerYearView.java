package com.kunzisoft.switchdatetime.date.widget;

import com.kunzisoft.switchdatetime.Utils;
import com.kunzisoft.switchdatetime.date.utils.AttrUtils;
import com.kunzisoft.switchdatetime.date.OnYearSelectedListener;
import com.kunzisoft.switchdatetime.date.utils.LogUtil;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;


/**
 * ListView for select one year, year selected is highlight <br />
 * To get the year, assign an OnYearSelectedListener
 *
 * @author JJamet
 * @see OnYearSelectedListener#onYearSelected(Component, int)
 */
public class ListPickerYearView extends ListContainer implements YearPickerAdapter.OnClickYearListener {

    private final static String TAG = "ListPickerYearView";
    private int minYear = 1970;
    private int maxYear = 2100;
    private int currentYear;

    private YearPickerAdapter mAdapter;
    private OnYearSelectedListener yearChangeListener;
    private int mChildSize;
    private int mViewSize;

    /**
     * 构造方法
     *
     * @param context context
     */
    public ListPickerYearView(Context context) {
        this(context, null, 0);
    }

    /**
     * 构造方法
     *
     * @param context context
     * @param attrs attrs
     */
    public ListPickerYearView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * 构造方法
     *
     * @param context context
     * @param attrs attrs
     * @param defStyleAttr defStyleAttr
     */
    public ListPickerYearView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, "");
        init(context, attrs);
    }

    private void init(Context context, AttrSet attrs) {
        setLayoutManager(new DirectionalLayoutManager());

        if (attrs != null) {
            setMinYear(AttrUtils.getIntFromAttr(attrs, "minYear", minYear));
            setMaxYear(AttrUtils.getIntFromAttr(attrs, "maxYear", minYear));
            currentYear = AttrUtils.getIntFromAttr(attrs, "defaultYear", YearPickerAdapter.UNDEFINED);
        }

        setLayoutConfig(new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT));

        mViewSize = Utils.vp2px(context, 270);
        mChildSize = Utils.vp2px(context, 64);
        mAdapter = new YearPickerAdapter(context);
        setItemProvider(mAdapter);

        mAdapter.setOnClickYearListener(this);

        refreshAndCenter();
    }

    /**
     * Assign years to adapter only if view is init
     */
    private void injectYearsInAdapter() {
        if (mAdapter != null && minYear <= maxYear) {
            ArrayList<Integer> years = new ArrayList<>();
            for (int year = minYear; year <= maxYear; year++) {
                years.add(year);
            }
            mAdapter.setListYears(years);
            mAdapter.notifyDataSetItemRangeChanged(0, years.size() - 1);
        }
    }

    /**
     * Refresh list and center on the selected year
     */
    public void refreshAndCenter() {
        mAdapter.notifyDataChanged();
        centerListOn(currentYear - minYear - 1);
    }

    @Override
    public void onItemYearClick(Component view, Integer year, int position) {
        int positionOldYear = mAdapter.getPositionSelectedYear();
        currentYear = year;
        if (yearChangeListener != null) {
            yearChangeListener.onYearSelected(view, year);
        }

        try {
            mAdapter.setSelectedYear(currentYear);
        } catch (YearPickerAdapter.SelectYearException e) {

            e.fillInStackTrace();
        }
        mAdapter.notifyDataChanged();
        mAdapter.notifyDataSetItemChanged(positionOldYear);
        mAdapter.notifyDataSetItemChanged(position);
    }

    /**
     * Center list on the selected year
     *
     * @param position of year in the list
     */
    public void centerListOn(final int position) {
        scrollTo(position);
        try {
            scrollTo(0, mViewSize / 2 - mChildSize / 2);

        } catch (Exception e) {
            LogUtil.w("Can't scroll more");
        }
    }

    /**
     * Attach listener for select year
     *
     * @param onYearSelectedListener listener
     */
    public void setDatePickerListener(OnYearSelectedListener onYearSelectedListener) {
        this.yearChangeListener = onYearSelectedListener;
    }

    /**
     * Get current minYear
     *
     * @return minYear minYear
     */
    public int getMinYear() {
        return minYear;
    }

    /**
     * Set minimum year of list
     *
     * @param minYear minimum year
     */
    public void setMinYear(int minYear) {
        this.minYear = minYear;
        injectYearsInAdapter();
    }

    /**
     * Get maximum year of list
     *
     * @return maxYear maxYear
     */
    public int getMaxYear() {
        return maxYear;
    }

    /**
     * Set maximum year of list
     *
     * @param maxYear maxYear
     */
    public void setMaxYear(int maxYear) {
        this.maxYear = maxYear;
        injectYearsInAdapter();
    }

    /**
     * Get current year selected
     *
     * @return currentYear currentYear
     */
    public int getYearSelected() {
        return currentYear;
    }

    /**
     * Assign current year and refresh list
     *
     * @param year
     */
    public void assignCurrentYear(int year) {
        currentYear = year;
        if (mAdapter != null) {
            try {
                mAdapter.setSelectedYear(currentYear);
            } catch (YearPickerAdapter.SelectYearException e) {
                e.fillInStackTrace();
            }
        }
        refreshAndCenter();
    }
}