/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunzisoft.switchdatetime.date;

import com.kunzisoft.switchdatetime.ResourceTable;
import com.kunzisoft.switchdatetime.event.MessageEvent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarManager {
    private List<Text> mTexts = new ArrayList<>();

    private int original_year;
    private int original_month;
    private int original_day;
    private int current_month = 0;
    private int firstWeekOffset = 0;
    private int offset = 0;
    private int month;
    private boolean isCurrentMonth;
    private int year;
    private Preferences mSelDatePreference;
    private boolean isOrientation;

    public void setCalendarManager(Component component, Calendar calendar) {
        // 获取保存的手机横竖屏情况（true - 竖屏；false - 横屏）
        DatabaseHelper databaseHelper = new DatabaseHelper(component.getContext());
        Preferences orientation = databaseHelper.getPreferences("ori_pref");
        isOrientation = orientation.getBoolean("orientation", true);
        mSelDatePreference = databaseHelper.getPreferences("sel_date");

        // 初始化布局
        findTexts(component, ResourceTable.Id_include_first);
        findTexts(component, ResourceTable.Id_include_second);
        findTexts(component, ResourceTable.Id_include_third);
        findTexts(component, ResourceTable.Id_include_forth);
        findTexts(component, ResourceTable.Id_include_five);
        findTexts(component, ResourceTable.Id_include_six);

        // 初始化数据
        initData(calendar);
    }

    private void findTexts(Component component, int resId) {
        DirectionalLayout include = (DirectionalLayout) component.findComponentById(resId);
        mTexts.add((Text) include.findComponentById(ResourceTable.Id_day_sun));
        mTexts.add((Text) include.findComponentById(ResourceTable.Id_day_mon));
        mTexts.add((Text) include.findComponentById(ResourceTable.Id_day_tue));
        mTexts.add((Text) include.findComponentById(ResourceTable.Id_day_web));
        mTexts.add((Text) include.findComponentById(ResourceTable.Id_day_thu));
        mTexts.add((Text) include.findComponentById(ResourceTable.Id_day_fri));
        mTexts.add((Text) include.findComponentById(ResourceTable.Id_day_sat));
    }

    public void initData(Calendar calendar) {
        original_year = calendar.get(Calendar.YEAR);
        original_month = calendar.get(Calendar.MONTH);
        original_day = calendar.get(Calendar.DAY_OF_MONTH);
        current_month = calendar.get(Calendar.MONTH);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        firstWeekOffset = Calendar.SATURDAY - dayOfWeek + 1;
        offset = firstWeekOffset - 7;
        for (int i = 0; i < 42; i++) {
            calendar.set(original_year, original_month, original_day);
            calendar.add(Calendar.DAY_OF_YEAR, offset);
            offset++;
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            isCurrentMonth = isCurrentMonth(month);
            Text text = mTexts.get(i);
            text.setText(day + "");

            if (year == MessageEvent.selsectYearInt && month == MessageEvent.selsectMonthInt && day == MessageEvent.selsectDayInt) {
                text.setTextColor(Color.WHITE);
                setTxtBg(text);
            } else {
                if (!isCurrentMonth) {
                    text.setTextColor(Color.GRAY);
                    text.setEnabled(false);
                } else {
                    text.setEnabled(true);
                    text.setTextColor(Color.BLACK);
                }
                text.setBackground(null);
            }

            text.setClickedListener(component -> {
                clearState();
                Text contentTxt = (Text) component;
                contentTxt.setTextColor(Color.WHITE);
                setTxtBg(contentTxt);
                MessageEvent.selsectYearInt = MessageEvent.yearInt;
                MessageEvent.selsectMonthInt = MessageEvent.monthInt;
                MessageEvent.selsectDayInt = Integer.parseInt(contentTxt.getText());
                EventBus.getDefault().post(new MessageEvent(contentTxt.getText()));
            });
        }
    }

    private void clearState() {
        for (int i = 0; i < 42; i++) {
            Text text = mTexts.get(i);
            Color color = text.getTextColor();
            if (color.getValue() == Color.WHITE.getValue()) {
                text.setTextColor(Color.BLACK);
            }
            text.setBackground(null);
        }
    }

    private void setTxtBg(Text text) {
        try {
            //获取Media文件夹中的图片资源
            Resource bgResource = text.getResourceManager().getResource(ResourceTable.Media_round);

            //根据资源生成PixelMapElement实例
            PixelMapElement pixBg = new PixelMapElement(bgResource);
            text.setBackground(pixBg);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }

    }

    private boolean isCurrentMonth(int month) {
        return current_month == month;
    }
}
