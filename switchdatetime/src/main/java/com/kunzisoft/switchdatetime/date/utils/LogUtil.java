/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunzisoft.switchdatetime.date.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class LogUtil {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    /**
     * 打印数据信息
     *
     * @param value value
     */
    public static void d(String value) {
        HiLog.debug(LABEL, "输出的值：             " + value);
    }

    /**
     * 打印数据信息
     *
     * @param value value
     */
    public static void i(String value) {
        HiLog.info(LABEL, "输出的值：             " + value);
    }

    /**
     * 打印数据信息
     *
     * @param value value
     */
    public static void w(String value) {
        HiLog.warn(LABEL, "输出的值：             " + value);
    }

    /**
     * 打印数据信息
     *
     * @param value value
     */
    public static void e(String value) {
        HiLog.error(LABEL, "输出的值：             " + value);
    }
}
