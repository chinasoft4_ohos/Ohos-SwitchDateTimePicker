/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunzisoft.switchdatetime.provider;

import com.kunzisoft.switchdatetime.date.CalendarManager;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.Calendar;
import java.util.List;

public class PageProvider extends PageSliderProvider {
    private Context mContext;
    private Calendar original_calendar;

    private List<Object> mViews;

    private int mOldPosition = -1;

    /**
     * constructor
     *
     * @param context context
     * @param calendar calendar
     * @param views 缓存view
     */
    public PageProvider(Context context, Calendar calendar, List<Object> views) {
        mContext = context;
        original_calendar = calendar;
        mViews = views;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        int index = i % mViews.size();
        DirectionalLayout rootLayout = (DirectionalLayout) mViews.get(index);
        Object obj = rootLayout.getTag();
        if (obj instanceof CalendarManager) {
            CalendarManager calendarManager = (CalendarManager) obj;
            Calendar calendar = calculateDate(original_calendar, i);
            calendarManager.initData(calendar);
        }

        componentContainer.addComponent(rootLayout);

        return rootLayout;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        DirectionalLayout view = (DirectionalLayout) o;
        componentContainer.removeComponent(view);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return false;
    }

    public static Calendar calculateDate(Calendar original_calendar, int monthOffset) {
        Calendar calendar = Calendar.getInstance();
        int year = original_calendar.get(Calendar.YEAR);
        int month = original_calendar.get(Calendar.MONTH);
        int day = original_calendar.get(Calendar.DAY_OF_MONTH);
        month = month + monthOffset;
        if (month > 11) {
            int times = (month + 1) / 12;
            year = year + times;
            month = month - 12 * times;
        }
        calendar.set(year, month, day);
        return calendar;
    }
}
